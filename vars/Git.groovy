def call(body){

	def config = [:]
	body.delegate = config
        body()
parallel BitBucket: {
              // Pulling
              git branch: "${config.bitbucket_branch}"
            VERSION = sh returnStdout: true, script: 'git describe --always'
                if (VERSION) {
                  echo "Building version ${VERSION}"
                }}, StatusNotify: {
                // Update bitbucketStatus of current commit
                bitbucketStatusNotify ( buildState: 'INPROGRESS' )
                hygieiaBuildPublishStep buildStatus: 'InProgress'
            }
        }